import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.yellow.shade700,
        ));
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.yellow.shade700,
        ));
  }
}

var mainapp = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(5, 50, 5, 5),
                  child: Text(
                    "24 ํ",
                    style: TextStyle(fontSize: 60),
                  )),
              Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Text(
                    "อำเภอเมืองชลบุรี",
                    style: TextStyle(fontSize: 30),
                  )),
              Padding(
                  padding: EdgeInsets.all(4.0), child: Icon(Icons.nightlight)),
              Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Text(
                    "29 ํ/21 ํ อุณหภูมิที่รู้สึก 24 ํ",
                    style: TextStyle(fontSize: 20),
                  )),
              Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Text(
                    "อ.,00.59",
                    style: TextStyle(fontSize: 15),
                  )),
            ],
          ),
        ),

        // Divider(
        //   color: Colors.grey,
        // ),

        Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.white,
                ),
              ),
              child: todayWeather(),
            )),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        today(),
        Divider(
          color: Colors.grey,
        ),
        // monday(),
        // Divider(
        //   color: Colors.grey,
        // ),
        // tuesday(),
        // Divider(
        //   color: Colors.grey,
        // ),
        // wednesday(),
        // Divider(
        //   color: Colors.grey,
        // ),
        // thursday(),
        // Divider(
        //   color: Colors.grey,
        // ),
        // friday(),
        // Divider(
        //   color: Colors.grey,
        // ),
        // saturday(),
        // Divider(
        //   color: Colors.grey,
        // ),
        // sunUpDown(),
      ],
    )
  ],
);





















class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return 
DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
      useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        debugShowCheckedModeBanner: false,
        theme: currentTheme == APP_THEME.DARK
            ? MyAppTheme.appThemeLight()
            : MyAppTheme.appThemeDark(),
        home: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  // image: AssetImage(
                  //     "https://t1.blockdit.com/photos/2019/08/5d594aee6af23d1609819d0f_800x0xcover_UZ332feu.jpg"),
                  image: NetworkImage(
                      "https://pbs.twimg.com/media/FEozZ8CVUAEjcTt?format=jpg&name=large"),
                  fit: BoxFit.cover)),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: mainapp,
            // floatingActionButton: FloatingActionButton(
            //   child: Icon(Icons.threesixty),
            //   onPressed: () {
            //     setState(() {
            //       currentTheme == APP_THEME.DARK
            //           ? currentTheme = APP_THEME.LIGHT
            //           : currentTheme = APP_THEME.DARK;
            //     });
            //   },
            // ),
          ),
        ),
        )
    );
  }
}

Widget am00() {
  return Column(
    children: <Widget>[
      Text("00.00"),
      IconButton(
        icon: Icon(
          Icons.circle,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("23 ํ"),
    ],
  );
}

Widget am01() {
  return Column(
    children: <Widget>[
      Text("01.00"),
      IconButton(
        icon: Icon(
          Icons.circle,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("23 ํ"),
    ],
  );
}

Widget am02() {
  return Column(
    children: <Widget>[
      Text("02.00"),
      IconButton(
        icon: Icon(
          Icons.nightlight_sharp,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("22 ํ"),
    ],
  );
}

Widget am03() {
  return Column(
    children: <Widget>[
      Text("03.00"),
      IconButton(
        icon: Icon(
          Icons.nightlight_round,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("22 ํ"),
    ],
  );
}

Widget am04() {
  return Column(
    children: <Widget>[
      Text("04.00"),
      IconButton(
        icon: Icon(
          Icons.nightlight_round_sharp,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("22 ํ"),
    ],
  );
}

Widget am05() {
  return Column(
    children: <Widget>[
      Text("05.00"),
      IconButton(
        icon: Icon(
          Icons.nightlight_round_rounded,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("21 ํ"),
    ],
  );
}

Widget am06() {
  return Column(
    children: <Widget>[
      Text("06.00"),
      IconButton(
        icon: Icon(
          Icons.sunny_snowing,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("20 ํ"),
    ],
  );
}

Widget am07() {
  return Column(
    children: <Widget>[
      Text("07.00"),
      IconButton(
        icon: Icon(
          Icons.sunny_snowing,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("21 ํ"),
    ],
  );
}

Widget am08() {
  return Column(
    children: <Widget>[
      Text("08.00"),
      IconButton(
        icon: Icon(
          Icons.sunny,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("23 ํ"),
    ],
  );
}

Widget am09() {
  return Column(
    children: <Widget>[
      Text("09.00"),
      IconButton(
        icon: Icon(
          Icons.sunny,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("23 ํ"),
    ],
  );
}

Widget am10() {
  return Column(
    children: <Widget>[
      Text("10.00"),
      IconButton(
        icon: Icon(
          Icons.sunny,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("24 ํ"),
    ],
  );
}

Widget am11() {
  return Column(
    children: <Widget>[
      Text("11.00"),
      IconButton(
        icon: Icon(
          Icons.sunny_snowing,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("23 ํ"),
    ],
  );
}

Widget am12() {
  return Column(
    children: <Widget>[
      Text("12.00"),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
        ),
        color: Colors.yellow.shade700,
        onPressed: () {},
      ),
      Text("22 ํ"),
    ],
  );
}

Widget today() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text(
        "วันนี้",
        style: TextStyle(fontSize: 20),
      ),
      Icon(Icons.nightlight, color: Colors.yellow.shade700),
      Icon(Icons.cloud, color: Colors.lightBlue),
      Text(
        "29 ํ",
        style: TextStyle(fontSize: 20),
      ),
      Text(
        "22 ํ",
        style: TextStyle(fontSize: 20),
      ),
    ],
  );
}

Widget monday() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text(
        "จันทร์",
        style: TextStyle(fontSize: 20),
      ),
      Icon(Icons.nightlight, color: Colors.yellow.shade700),
      Icon(Icons.cloud, color: Colors.lightBlue),
      Text(
        "29 ํ",
        style: TextStyle(fontSize: 20),
      ),
      Text(
        "21 ํ",
        style: TextStyle(fontSize: 20),
      ),
    ],
  );
}

Widget tuesday() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text(
        "อังคาร",
        style: TextStyle(fontSize: 20),
      ),
      Icon(Icons.nightlight, color: Colors.yellow.shade700),
      Icon(Icons.cloud, color: Colors.lightBlue),
      Text(
        "30 ํ",
        style: TextStyle(fontSize: 20),
      ),
      Text(
        "23 ํ",
        style: TextStyle(fontSize: 20),
      ),
    ],
  );
}

Widget wednesday() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text(
        "พุธ",
        style: TextStyle(fontSize: 20),
      ),
      Icon(Icons.nightlight, color: Colors.yellow.shade700),
      Icon(Icons.cloud, color: Colors.lightBlue),
      Text(
        "31 ํ",
        style: TextStyle(fontSize: 20),
      ),
      Text(
        "23 ํ",
        style: TextStyle(fontSize: 20),
      ),
    ],
  );
}

Widget thursday() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text(
        "พฤหัสบดี",
        style: TextStyle(fontSize: 20),
      ),
      Icon(Icons.nightlight, color: Colors.yellow.shade700),
      Icon(Icons.cloud, color: Colors.lightBlue),
      Text(
        "30 ํ",
        style: TextStyle(fontSize: 20),
      ),
      Text(
        "23 ํ",
        style: TextStyle(fontSize: 20),
      ),
    ],
  );
}

Widget friday() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text(
        "ศุกร์",
        style: TextStyle(fontSize: 20),
      ),
      Icon(Icons.nightlight, color: Colors.yellow.shade700),
      Icon(Icons.cloud, color: Colors.lightBlue),
      Text(
        "28 ํ",
        style: TextStyle(fontSize: 20),
      ),
      Text(
        "21 ํ",
        style: TextStyle(fontSize: 20),
      ),
    ],
  );
}

Widget saturday() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Text(
        "เสาร์",
        style: TextStyle(fontSize: 20),
      ),
      Icon(Icons.nightlight, color: Colors.yellow.shade700),
      Icon(Icons.cloud, color: Colors.lightBlue),
      Text(
        "27 ํ",
        style: TextStyle(fontSize: 20),
      ),
      Text(
        "21 ํ",
        style: TextStyle(fontSize: 20),
      ),
    ],
  );
}

// Widget sunUpDown() {
//   return Row(children: <Widget>[
//     Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
//       Padding(
//           padding: EdgeInsets.fromLTRB(50, 50, 5, 5),
//           child: Text(
//             "พระอาทิตย์ขึ้น",
//             style: TextStyle(fontSize: 20),
//           )),
//       Padding(
//         padding: EdgeInsets.fromLTRB(50, 50, 5, 5),
//         child: Text(
//           "พระอาทิตย์ลง",
//           style: TextStyle(fontSize: 20),
//         ),
//       )
//     ])
//   ]);
// }

Widget todayWeather() {
  return Container(
    child: SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          am00(),
          am01(),
          am02(),
          am03(),
          am04(),
          am05(),
          am06(),
          am07(),
          am08(),
          am09(),
          am10(),
          am11(),
          am12()
        ],
      ),
    ),
  );
}
